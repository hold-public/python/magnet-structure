import itertools
from enum import Enum

class Y_pos(Enum):
    NONINVERTED = 1
    INVERTED = 2
    UNDEF = 3

class Coord(object):
    def __init__(self, phi, y_pos):
        self.phi = phi
        self.y_pos = y_pos

possible_phis = list(range(0, 360, 45))
possible_y_pos = (Y_pos.NONINVERTED, Y_pos.INVERTED)
possible_coords = []
for phi in possible_phis:
    for y_pos in possible_y_pos:
        possible_coords.append(Coord(phi, y_pos))

def find_test_configs(sensor_nr):
    configs_to_test = []
    configs_to_test = iterate_rec(sensor_nr, configs_to_test, [])
    return configs_to_test
    	
def iterate_rec(sensor_nr, config_list, predefined_config_part):
    if len(predefined_config_part) < sensor_nr:
        for coord in possible_coords:
            new_config_part = predefined_config_part + [coord]
            config_list = iterate_rec(sensor_nr, config_list, new_config_part)
    else:
        config_list.append(predefined_config_part)
    return config_list

def test_configs(configs_to_test):
    nr_of_working_configs = 0
    for config in configs_to_test:
        config_works = test_one_config(config)
        if (config_works):
            print_config(config)
            nr_of_working_configs += 1
    return nr_of_working_configs

def test_one_config(config):
    config_works = False
    
    is_aligned_to_first_sensor = check_alignment_to_first_sensor(config)
    if is_aligned_to_first_sensor:
        is_correct_order = check_correct_order(config)
        if is_correct_order:
            magnetisation_codes_not_pressed = find_magnetisation_codes(config, False)
            magnetisation_codes_pressed = find_magnetisation_codes(config, True)
            magnetisation_codes = [magnetisation_codes_not_pressed, magnetisation_codes_pressed]

            magnetisation_codes_list = magnetisation_codes_not_pressed + magnetisation_codes_pressed
            has_no_duplicates = len(magnetisation_codes_list) == len(set(magnetisation_codes_list))
            if (has_no_duplicates):
                neighbours = find_neighbours(magnetisation_codes)
                transients = find_transients(magnetisation_codes, neighbours)
                ambiguous_codes, nr_of_ambiguious_codes = find_ambiguous_codes(magnetisation_codes, transients)
                if nr_of_ambiguious_codes == 0:
                    config_works = True
                else:
                    config_works = False
                    explain_ambiguity(magnetisation_codes, transients, ambiguous_codes)
            else:
                config_works = False
        else:
            config_works = False
    else:
        config_works = False
    return config_works

def check_alignment_to_first_sensor(config):
    if config[0].phi == 0:
        return True
    else:
        return False

def check_correct_order(config):
    last_phi = -1
    for sensor in config:
        if sensor.phi > last_phi:
            last_phi = sensor.phi 
        else:
            return False
    return True

def find_magnetisation_codes(config, button_pressed):
    magnetisation_values = []
    for sensor_index, sensor_zero_pos in enumerate(config):
        phi_offset = sensor_zero_pos.phi
        if not button_pressed:
            if sensor_zero_pos.y_pos == Y_pos.INVERTED:
                phi_offset += 180
        signal_flow = [(phi + phi_offset) % 360 for phi in possible_phis]
        magnetisation_value = [(2 ** sensor_index) * (this_flow < 180) for this_flow in signal_flow]
        magnetisation_values.append(magnetisation_value)

    magnetisation_codes = [sum(i) for i in zip(*magnetisation_values)]
    return list(magnetisation_codes)

def find_neighbours(magnetisation_codes):
    neighbours = [find_neighbours_linewise(magnetisation_codes[0], magnetisation_codes[1]), find_neighbours_linewise(magnetisation_codes[1], magnetisation_codes[0])]
    return neighbours

def find_neighbours_linewise(this_line, other_line):
    neighbours_left = this_line[-1:] + this_line[:-1]
    neighbours_right = this_line[1:] + this_line[:1]
    neighbours_other_side = other_line
    neighbours = list(zip(neighbours_left, neighbours_right, neighbours_other_side))
    return neighbours

def find_transients(me_array, neighbours_array):
    transients_array = []
    for me_row, neighbours_row in zip(me_array, neighbours_array):
        transients_row = []
        for me, neighbours in zip(me_row, neighbours_row):
            transients = []
            for neighbour in neighbours:
                transients_to_this_neighbour = find_transients_between_two_numbers(me, neighbour)
                transients_to_this_neighbour.remove(me)
                transients.extend(transients_to_this_neighbour)
            transients_row.append(transients)
        transients_array.append(transients_row)
    return transients_array

def find_transients_between_two_numbers(num_a, num_b):
    different_bits = num_a ^ num_b
    common_base = num_a & ~different_bits
    nr_of_bits = max(num_a.bit_length(), num_b.bit_length())

    transients_non_unique = []
    for diff in range(2 ** nr_of_bits):
        this_transient = common_base + (diff & different_bits)
        transients_non_unique.append(this_transient)

    transients = list(set(transients_non_unique))
    return transients

def find_ambiguous_codes(magnetisation_codes, transients):
    ambiguous_codes_array = []
    nr_of_ambiguious_codes = 0
    for code_row, transient_row in zip(magnetisation_codes, transients):
        ambiguous_codes_row = []
        for code, transients in zip(code_row, transient_row):
            double_transients = find_duplicates(transients)
            if double_transients:
                nr_of_ambiguious_codes += 1
            ambiguous_codes = dict()
            ambiguous_codes['code'] = code
            ambiguous_codes['transients'] = double_transients
            ambiguous_codes_row.append(ambiguous_codes)
        ambiguous_codes_array.append(ambiguous_codes_row)
    return ambiguous_codes_array, nr_of_ambiguious_codes

def find_duplicates(in_list):
    seen = {}
    dupes = []

    for x in in_list:
        if x not in seen:
            seen[x] = 1
        else:
            if seen[x] == 1:
                dupes.append(x)
            seen[x] += 1
    return dupes

def explain_ambiguity(magnetisation_codes, transients, ambiguous_codes):
    print('codes:')
    print(magnetisation_codes[0])
    print(magnetisation_codes[1])
    print('transients:')
    print(transients[0])
    print(transients[1])
    print('does not work because: ')
    for row in ambiguous_codes:
        for element in row:
            print('code ' + str(element['code']) + ' has those ambiguous transients: ' + str(element['transients']))


def print_config(config):
    print('config: ')
    sensor_nr = 1
    for sensor in config:
        if sensor.y_pos == Y_pos.NONINVERTED:
            y_pos_str = '+'
        else:
            y_pos_str = '-'
        print('sensor ' + str(sensor_nr) + ': ' + str(sensor.phi) + '°' + y_pos_str)
        sensor_nr += 1
    print('')

def get_fixed_config():
    c1 = Coord(0 * 45, Y_pos.NONINVERTED)
    c2 = Coord(6 * 45, Y_pos.NONINVERTED)
    c3 = Coord(3 * 45, Y_pos.INVERTED)
    c4 = Coord(5 * 45, Y_pos.INVERTED)

    return [[c1, c2, c3, c4]]

sensor_nr = 4
print('Test with ' + str(sensor_nr) + ' sensors ~~~~~~~~~~~~~~')
print('finding test configs...')
#configs_to_test = get_fixed_config()
configs_to_test = find_test_configs(sensor_nr)
print('testing found configs...')
nr_of_working_configs = test_configs(configs_to_test)
if (nr_of_working_configs > 0):
    print('nr of working configs: ' + str(nr_of_working_configs))
else:
    print('no working configs found... :[')
print('finished!')